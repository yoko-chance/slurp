package slurp

import (
	"os"
	"testing"
)

func TestInvokeHandler(t *testing.T) {
	var msgs = [...]string{
		"THE BIRDS waged war with the Beasts, and each were by turns the conquerors.",
		"A Bat, fearing the uncertain issues of the fight, always fought on the side which he felt was the strongest.",
		"When peace was proclaimed, his deceitful conduct was apparent to both combatants.",
		"Therefore being condemned by each for his treachery,",
		"he was driven forth from the light of day,",
		"and henceforth concealed himself in dark hiding-places,",
		"flying always alone and at night.",
	}

	msg := ""
	for _, m := range msgs {
		msg += m + "\n"
	}

	tmp, _ := os.CreateTemp(os.TempDir(), "dummy-input")
	defer os.Remove(tmp.Name())
	err := os.WriteFile(tmp.Name(), []byte(msg), 0644)
	if err != nil {
		t.Fatal(err)
	}

	n := 0
	ReadLines(tmp, func() Handler {
		return func(l string) error {
			if l != msgs[n] {
				t.Fatalf("expect '%s', actual '%s'", msgs[n], l)
			}

			n += 1

			return nil
		}
	}())

	if n != len(msgs) {
		t.Fatalf("expect %d, actual %d", len(msgs), n)
	}
}
