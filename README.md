Slurp
=====
Slurpは入力を1行ずつ処理するための簡素なフレームワークです

Motivation
----------
標準入力を1行ずつ処理する単純なアプリケーションをより簡素に書くために作成しました

Usage
-----
受け取った標準入力をそのまま出力するアプリケーションは以下のように実装できます

```go
package main

import (
	"gitlab.com/yoko-chance/slurp"
	"golang.org/x/crypto/ssh/terminal"
	"fmt"
	"os"
)

func main() {
	if terminal.IsTerminal(0) {
		return
	}

	slurp.ReadLines(os.Stdin, handler)
}

func handler(l string) error {
	fmt.Println(l)
	return nil
}
```
