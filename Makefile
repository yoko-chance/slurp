APP_NAME ?= $(notdir $(patsubst %/,%,$(CURDIR)))
MODULE := $(shell git remote get-url origin | \sed -e 's;^..*://\(..*@\)*\(..*\)\.git;\2;')

.DEFAULT_GOAL := help

.PHONY: test
test: ## test
	@docker run --rm -it \
		-v "$(CURDIR):/$(APP_NAME)" \
		-w "/$(APP_NAME)" \
		golang:1.15.6-alpine3.12 \
		go test

.PHONY: init
init: ## init application
	@docker run --rm -it \
		-v "$(CURDIR):/$(APP_NAME)" \
		-w "/$(APP_NAME)" \
		golang:1.15.6-alpine3.12 \
		go mod init $(MODULE)

.PHONY: help
help: ## display this text. refer to https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-10s\033[0m : %s\n", $$1, $$2}'
