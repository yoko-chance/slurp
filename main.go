package slurp

import (
	"bufio"
	"io"
	"log"
	"strings"
)

type (
	// Handler receive string and do any processes
	Handler func(l string) error
)

// ReadLines reads string from io.Reader and passes it to Handler
func ReadLines(rd io.Reader, f Handler) {
	r := bufio.NewReader(rd)
	for {
		l, err := r.ReadString('\n')

		if err == io.EOF {
			break

		} else if err != nil {
			log.Fatal(err)
		}

		if err := f(strings.Trim(l, "\n")); err != nil {
			log.Fatal(err)
		}
	}
}
